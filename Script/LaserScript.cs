﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserScript : MonoBehaviour {

	Rigidbody2D LaserRB;
	public int speed;

	void Start()
	{
		LaserRB = GetComponent<Rigidbody2D> ();
		Destroy (gameObject, 0.5f);
	}

	// Update is called once per frame
	void Update () 
	{
		LaserRB.velocity = new Vector2 (5 * speed, 0);
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.gameObject.tag == "Aliens")
		{
			col.gameObject.GetComponent<AlienScript> ().DamageLaser ();
		}
	}
}
