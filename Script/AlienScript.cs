﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlienScript : MonoBehaviour {

	Rigidbody2D AlienRB;
	public GameObject missile, explosion, bonusvie;
	public Color colormissile;

	[Range(0f,10f)]public float xSpeed;
	[Range(0f,10f)]public float ySpeed;
	public int score, chancedrop;

	public bool canShoot;
	public float fireRate;
	public float PV;

	void Awake()
	{
		AlienRB = GetComponent<Rigidbody2D> ();
	}

	// Use this for initialization
	void Start () 
	{
		if (!canShoot)
			return;
			fireRate = fireRate+(Random.Range(fireRate/-2,fireRate/2));
			InvokeRepeating ("Shoot", fireRate, fireRate);

		Destroy (gameObject, 20);
	}

	// Update is called once per frame
	void Update () 
	{
		AlienRB.velocity = new Vector2 (xSpeed*-1, ySpeed);
	}

	void OnCollisionEnter2D(Collision2D col)
	{
		if (col.gameObject.tag == "Player")
		{
			col.gameObject.GetComponent<VaisseauScript> ().Damage();
			BOUM ();
		}
			
	}

	void BOUM()
	{
		if ((int)Random.Range (0,chancedrop) == 0) 
		{
			Instantiate (bonusvie, transform.position, Quaternion.identity);
		}
		Instantiate (explosion, transform.position, Quaternion.identity);
		PlayerPrefs.SetInt("Score",PlayerPrefs.GetInt("Score")+score);
		Destroy (gameObject);
	}

	public void Damage ()
	{
		PV--;
		if (PV == 0) 
		{
			BOUM ();
		}
	}

	public void DamageLaser()
	{
		PV = PV - 0.5f;
		if (PV == 0) {
			BOUM ();
		}
	}

	void Shoot()
	{
		GameObject temp = (GameObject)Instantiate(missile, transform.position, Quaternion.identity);
		temp.GetComponent<MissileAlienScript>().ChangeDirection ();
		temp.GetComponent<MissileAlienScript> ().ChangeColor (colormissile);
	}
}
