﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VaisseauScript : MonoBehaviour {


	int delay = 0;
	public int delaytir;
	public int delaytir1;
	GameObject Canon1, Canon2, Canon3, CanonUltime;
	public GameObject missile, laser, explosion;
	Rigidbody2D VaisseauRB;
	[Range(0.5f,20f)]public float speed;
	public int HP;
	public AudioClip missilesound;
		
	// Use this for initialization
	void Start () 
	{
		PlayerPrefs.SetInt ("HP", HP);
		VaisseauRB = GetComponent<Rigidbody2D> ();
		Canon1 = transform.Find ("Canon1").gameObject;
		Canon2 = transform.Find ("Canon2").gameObject;
		Canon3 = transform.Find ("Canon3").gameObject;
		CanonUltime = transform.Find ("CanonUltime").gameObject;
	}
	
	// Update is called once per frame
	void Update ()
	{
		Vector3 mouvement = Vector3.zero;
		mouvement.x += speed * Input.GetAxisRaw("Horizontal") * Time.fixedDeltaTime;
		mouvement.y += speed * Input.GetAxisRaw ("Vertical") * Time.fixedDeltaTime;

		if (transform.position.x + mouvement.x > 8.5f || transform.position.x + mouvement.x < -8.5f)
			mouvement.x = 0;

		if (transform.position.y + mouvement.y > 4.5f || transform.position.y + mouvement.y < -4.5f)
			mouvement.y = 0;

		if (mouvement.magnitude > 1f)
			mouvement.Normalize ();

		VaisseauRB.MovePosition (transform.position + mouvement);

		if (Input.GetKey(KeyCode.Space)&&delay>delaytir) // SECOND TIR POSSIBLE ICI 
		{
			AudioSource.PlayClipAtPoint (missilesound, Camera.main.transform.position);
			Tir ();
		}
		if (Input.GetKey(KeyCode.V)&&delay>delaytir1) // SECOND TIR POSSIBLE ICI 
		{
			Tir2 ();
		}

		delay++;
			
	}

	public void Damage ()
	{
		HP--;
		PlayerPrefs.SetInt ("HP", HP);
		StartCoroutine (Blink ());
		if (HP == 0) 
		{
			Instantiate (explosion, transform.position, Quaternion.identity);
			Destroy (gameObject,0.1f);
		}
	}

	IEnumerator Blink()
	{
		GetComponent<SpriteRenderer>().color = new Color (1, 0, 0);
		yield return new WaitForSeconds (0.2f);
		GetComponent<SpriteRenderer>().color = new Color (1, 1, 1);
		yield return new WaitForSeconds (0.2f);
	}

	void Tir()
	{
		delay = 0;
		Instantiate (missile, Canon1.transform.position, Quaternion.identity);
		Instantiate (missile, Canon2.transform.position, Quaternion.identity);
		Instantiate (missile, Canon3.transform.position, Quaternion.identity);
	}
	void Tir2()
	{
		Instantiate (laser, CanonUltime.transform.position, Quaternion.identity);
	}

	public void AddHP()
	{
		HP++;
		PlayerPrefs.SetInt ("HP", HP);
	}
}
