﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileAlienScript : MonoBehaviour {

	Rigidbody2D MissileRB;
	int dir=-1;
	public float speed;


	void Start()
	{
		MissileRB = GetComponent<Rigidbody2D> ();
	}

	public void ChangeDirection()
	{
		dir*=1;
	}

	public void ChangeColor(Color col)
	{
		GetComponent<SpriteRenderer> ().color = col;
	}
		
	// Update is called once per frame
	void Update () 
	{
		MissileRB.velocity = new Vector2 (speed * dir, 0);
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		if(dir==-1){
			if (col.gameObject.tag == "Player") {
				col.gameObject.GetComponent<VaisseauScript> ().Damage ();
				Destroy (gameObject);
			}
		}
	}
}