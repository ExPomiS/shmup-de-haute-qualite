﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndGameScript : MonoBehaviour {

	GameObject joueur;
	bool gameOver=false;

	void Start () 
	{
		joueur = GameObject.FindGameObjectWithTag ("Player");
	}
	

	void Update () 
	{
		if (joueur == null&&!gameOver)
			GameOver ();
	}

	void GameOver()
	{
		gameOver = true;
		if(PlayerPrefs.GetInt("Score")>PlayerPrefs.GetInt("HighScore"))
		{
			PlayerPrefs.SetInt("HighScore",PlayerPrefs.GetInt("Score"));
		}
		StartCoroutine (LoadGameOver());
	}

	IEnumerator LoadGameOver()
	{
		yield return new WaitForSeconds(3f);
		SceneManager.LoadScene (2);
	}
}
