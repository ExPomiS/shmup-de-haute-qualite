﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnScript : MonoBehaviour {

	public float rateSpawn;
	public GameObject[] Aliens;
	public int waves;

	// Use this for initialization
	void Start () 
	{
		InvokeRepeating ("Spawn", rateSpawn, rateSpawn);
	}
	
	void Spawn () 
	{
		for (int i = 0; i < waves; i++)
		{
			Instantiate (Aliens [(int)Random.Range (0, Aliens.Length)], new Vector3 (9.5f, Random.Range (-5f, 5f), 0), Quaternion.identity);
		}
	}
}
