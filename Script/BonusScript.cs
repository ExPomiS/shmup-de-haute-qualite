﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusScript : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.gameObject.tag == "Player") 
		{
			col.gameObject.GetComponent<VaisseauScript>().AddHP ();
			Destroy (gameObject);
		}
	}
}
